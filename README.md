This program extracts variations in the spin-down rate of radio pulsars from pulsar timing residuals. The code uses Gaussian Process regression to model the timing residuals, the second derivative of which is calculated to be the spin-down rate. 

The required inputs are 
* -e A pulsar ephemeris (par file)
* -r A file of timing residuals <days since pepoch><residual><err on residual>

Residuals should be expressed in time rather than phase. 

The initial lengthscale hyperparameter is configurable in days. The default is 500d and this should be fine for most purposes. 

The value of nudot is currently only inferred at the observation epochs. The -c CADENCE option is yet to be implemented. 

TODO:
* Option to use multiple covariance functions

The code will produce a plot of the residuals as well as a file mjd_nudot_error.dat comprising 3 columns

* The number of days since the period epoch in the par file
* The value of nudot (1e-15 Hz)
* The uncertainty on nudot (1e-15 Hz)

The code is available in a docker image

```bash
docker pull pulsarben/gpnudot:latest
```

Once pulled, one can start a container using 

```bash
docker run -it pulsarben/gpnudot:latest
```

Once in the container, the executable should be in your path. Check this by running

```bash
gp_nudot.py -h
```

There's a test dataset available to verify the code works.

```bash
gp_nudot.py -r /home/gpnudot/test_data/residuals.dat -e /home/gpnudot/test_data/psr.eph
```


This code is based on Vpsr by Aris Karastergiou and Paul Brook and has similar functionality (https://github.com/ArisKarastergiou/Vpsr). For details see https://arxiv.org/abs/1511.05481
