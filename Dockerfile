FROM python:3.7

RUN apt-get -y update && \
    apt-get -u update && \
    apt-get install -y vim

WORKDIR /home/gpnudot/
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY gp_nudot.py gp_nudot.py
WORKDIR /home/gpnudot/test_data
COPY fixtures/* /home/gpnudot/test_data/
WORKDIR /home
RUN chmod 775 /home/gpnudot/gp_nudot.py
ENV PATH="/home/gpnudot:${PATH}"
ENTRYPOINT /bin/bash
